# Ingress Controller Classic migration from old version

## What we are trying to achieve?

  - We have an EKS on AWS.
  - We are using Classic Load Balancer (ELB)
  - We installed ELB a long time ago using Helm (chart version 1.7.0)
  - We want to upgrade the chart version to the last one and change some settings (chart version 1.29.1)
  - Since ELB starts a LoadBalancer, i.e. an address, and we need to target this address in chart's annotations, we want to keep it the same *


* _Always we can delete and recreate the ingress controller. In this case we will need to modify all the charts that includes the target annotation. The solution here is to just upgrade the ingress controller._

## What's the problem we are facing?

We can face here two issues.

The first one, helm not updating due to spec.ClusterIP is immutable.

```
╰─ helm upgrade --install  -f ingress-values.yaml migrationtestr-ingress-public --namespace canary --version 1.29.2 stable/nginx-ingress
Error: UPGRADE FAILED: Service "migrationtestr-ingress-public-nginx-ingress-controller" is invalid: spec.clusterIP: Invalid value: "": field is immutable && Service "migrationtestr-ingress-public-nginx-ingress-default-backend" is invalid: spec.clusterIP: Invalid value: "": field is immutable
```

The second one, Service Account already exist.

```
╰─ helm upgrade --install  -f ingress-values.yaml migrationtestr-ingress-public --namespace canary --version 1.29.2 stable/nginx-ingress
Error: UPGRADE FAILED: kind ServiceAccount with the name "migrationtestr-ingress-public-nginx-ingress-backend" already exists in the cluster and wasn't defined in the previous release. Before upgrading, please either delete the resource from the cluster or remove it from the chart
```

From here on, if you try again you will keep getting this second error.

## Why this happens?

The first issue was a known issue on older versions. But it was fixed a long time ago. I think, when helm tries to merge the revision manifests with the new ones, it brings the bug from the former ones.

The second arises from the fact that helm is failing and the deployment is half done. So I think the service account is created but, somehow, not reflected in the manifests stored by helm in the revision.

## Try it

### Prerequisites

You will need:

  - An EKS cluster (that you can broke)
  - An S3 bucket

In the values file, in line 195 change the bucket name to yours:

```
service.beta.kubernetes.io/aws-load-balancer-access-log-s3-bucket-name: "juankungfukube2iamtest001"
```

### Deploy it!

Now deploy the ingress using this command:

```
helm install -f ingress-values.yaml --name migrationtestr-ingress-public --namespace canary --version 1.7.0 stable/nginx-ingress
```

_Change your namespace to one that exists in your cluster._

Check the service and ensure a load balancer was assigned to your ingress controller:

```
╰─ kubectl get svc -n canary -o wide migrationtestr-ingress-public-nginx-ingress-controller
NAME                                                     TYPE           CLUSTER-IP       EXTERNAL-IP                                                              PORT(S)                      AGE     SELECTOR
migrationtestr-ingress-public-nginx-ingress-controller   LoadBalancer   999.999.999.999   ab50474013de811eaa3699999999-314773338.us-east-2.elb.amazonaws.com   80:32080/TCP,443:32103/TCP   2m23s   app=nginx-ingress,component=controller,release=migrationtestr-ingress-public
```

_(Yes, that IP does not exist... just changed to not expose mine)_

Uner _EXTERNAL-IP_ is your load balancer's address.

Also, if you submit this command: 

```
kubectl get svc -n canary migrationtestr-ingress-public-nginx-ingress-controller -o yaml
```

You will find that _ClusterIP_ has a value (your IP).

Check the revision history on helm:

```
╰─ helm history migrationtestr-ingress-public
REVISION	UPDATED                 	STATUS  	CHART              	DESCRIPTION     
1       	Thu Jan 23 14:01:56 2020	DEPLOYED	nginx-ingress-1.7.0	Install complete
```

### Now the upgrade

I will do a change in the values file, just because this was what we needed to do when we found this issue.

In values file, we needed to add the _use-forwarded-headers_ variable and set it to _true_. So let's go to values file and uncomment line 37. This var must be located under *config:*, so take care of your indentations.

Now upgrade your Ingress Controller:

```
helm upgrade --install  -f ingress-values.yaml migrationtestr-ingress-public --namespace canary --version 1.29.2 stable/nginx-ingress
```

And here we have an error:

```
Error: UPGRADE FAILED: Service "migrationtestr-ingress-public-nginx-ingress-controller" is invalid: spec.clusterIP: Invalid value: "": field is immutable && Service "migrationtestr-ingress-public-nginx-ingress-default-backend" is invalid: spec.clusterIP: Invalid value: "": field is immutable
```

If we check the history:

```
╰─ helm history migrationtestr-ingress-public
REVISION	UPDATED                 	STATUS  	CHART               	DESCRIPTION                                                 
1       	Thu Jan 23 14:01:56 2020	DEPLOYED	nginx-ingress-1.7.0 	Install complete                                            
2       	Thu Jan 23 14:15:58 2020	FAILED  	nginx-ingress-1.29.2	Upgrade "migrationtestr-ingress-public" failed: Service "...

```

From here on, if you try to upgrade again, you will have another error:

```
╰─ helm upgrade --install  -f ingress-values.yaml migrationtestr-ingress-public --namespace canary --version 1.29.2 stable/nginx-ingress
Error: UPGRADE FAILED: kind ServiceAccount with the name "migrationtestr-ingress-public-nginx-ingress-backend" already exists in the cluster and wasn't defined in the previous release. Before upgrading, please either delete the resource from the cluster or remove it from the chart
```

If you try again you will get the same error again and again.

## How to perform the upgrade then?

Based on this [comment](https://github.com/helm/charts/issues/9227#issuecomment-457136396) we will follow these steps:

  - Get the IPs for the services (we had an error on the controller one, but as soon as we solve this we will have a problem with the default backend, so...)
  - If you already tried to upgrade and it failed, delete the service account for backend (if your upgrade has failed, it's probable that the default backend service account was created, next deploy will fail trying to create it since it's not in the last manifest)
  - Specify the IPs on the next upgrade

### Get the IPs

```
MIP=$(kubectl -n canary get services migrationtestr-ingress-public-nginx-ingress-controller -o jsonpath="{.spec.clusterIP}") 

MIPB=$(kubectl -n canary get services migrationtestr-ingress-public-nginx-ingress-default-backend -o jsonpath="{.spec.clusterIP}")
```

_Change the name or the namespace as needed._

### Delete the serviceaccount

```
kubectl delete ServiceAccount migrationtestr-ingress-public-nginx-ingress-backend -n canary
```

If upgrade has failed previously, it's possible that you need to delete also the deployment and service for backend. In such a case the errors could be something like "ServiceAccount not found".


### Finally do the upgrade

```
helm upgrade -f ingress-values.yaml migrationtestr-ingress-public --namespace canary --version 1.29.2  --set controller.service.clusterIP=${MIP} --set defaultBackend.service.clusterIP=${MIPB} stable/nginx-ingress
```

That's all...

```
╰─ helm history migrationtestr-ingress-public
REVISION	UPDATED                 	STATUS    	CHART               	DESCRIPTION                                                 
1       	Thu Jan 23 14:01:56 2020	SUPERSEDED	nginx-ingress-1.7.0 	Install complete                                            
2       	Thu Jan 23 14:15:58 2020	FAILED    	nginx-ingress-1.29.2	Upgrade "migrationtestr-ingress-public" failed: Service "...
3       	Thu Jan 23 14:27:42 2020	DEPLOYED  	nginx-ingress-1.29.2	Upgrade complete                                     
```
